package com.singularitybd.smsinterceptapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        val prefrepository = PrefRepository(this)

        var url = prefrepository.getUrl();

        if (url.isBlank()) {
            prefrepository.saveUrl("http://bondstein.net/sinotrack/api_sms_recv.php");
            url = "http://bondstein.net/sinotrack/api_sms_recv.php";
        }

        txtUrl.text = url;

        btnUpdate.setOnClickListener {
            prefrepository.saveUrl(etUrl.text.toString())
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else                 -> super.onOptionsItemSelected(item)
        }
    }
}
