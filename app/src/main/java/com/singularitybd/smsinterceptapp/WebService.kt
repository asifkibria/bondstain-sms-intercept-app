package com.singularitybd.smsinterceptapp

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Sadman Sarar on 9/9/17.
 * Retrofit Service class
 */
interface WebService {

    @FormUrlEncoded
    @Headers("Accept: Application/json")
    @POST("/{path}")
    fun postSms(
            @Path(value = "path", encoded = true) path: String,
            @Field("data") data: String,
            @Field("number") number: String,
            @Field("time") time: String
    ): Call<String>


    /**
     * We need to make an app for State recognition for Tracker.

    API URL: http://bondstein.net/sinotrack/api_sms_recv.php
    SMS body text should be POST request.
    Post value name is "data"
    Return: Success Json Text

     */
}