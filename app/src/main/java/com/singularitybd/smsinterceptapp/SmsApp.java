package com.singularitybd.smsinterceptapp;

import android.app.Application;

import com.facebook.stetho.Stetho;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by Sadman Sarar on 5/9/18.
 */
public class SmsApp extends Application {
	
	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG) {
			Stetho.initializeWithDefaults(this);
		}
		JodaTimeAndroid.init(this);
		
	}
}
